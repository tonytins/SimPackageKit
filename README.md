# SimPackageKit

This is a port of SimPE's interfaces and helper libraries to Swift.

## Requirements

- Swift 5.3 or later
- macOS 10.12.6 or later

## License

I license this project under the BSD-3-Clause license - see [LICENSE](LICENSE) for details.
